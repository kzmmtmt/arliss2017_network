# v2 (17/7/31)
module Parameter

  # 探索開始点(ここから北・東方向に領域を生成)
  START_POINT=[] # 経度,緯度 # 空にするとユーザー入力モード
  
  # 個体設定
  begin
    MY_ID=(`cat /home/pi/id`).to_i # My Wi-Fi ID; You can not use 0
  rescue Exception
    MY_ID=-1
  end
  LOG=true # -dオプション時にログを取る
  PASSWORD="highball" # Wi-Fi password
  SSID_KEYWORD="mulcheese" # SSID prefix

  # ネットワーク関連
  PORT=9999 # UDPポート
  CHECK_INTERVAL=10 # sec スキャンする頻度
  AVAILABLE_TIME=10 # sec 最後に通信してから有効な時間
  SEND_INTERVAL=1 # sec 生存パケットの送信間隔
  CLEAN_INTERVAL=5 # sec テーブル更新間隔
  
  # ファイル関係
  TABLE_PATH="/home/pi/tables" # tableファイルの保存先
  PARENT_FLAG="/home/pi/parent_flag"
  LOG_PATH="/home/pi/" # ログ保存先のファイル
  GOAL_LIST="/home/pi/goal_list"
  MISSION_LOG="/home/pi/"

  # 経路関係
  TARGET_ID=0 # ターゲット対象のID(一番小さいほうが良い)
  BLOCK_WIDTH=10
  BLOCK_HEIGHT=10
  BLOCK_SIZE_HORIZONTAL=10
  BLOCK_SIZE_VERTICAL=10
  SHARING_INTERVAL=3 # 経路の共有間隔
  ROUTE_PORT=10000
  SELF_MONITORING_PORT=10001
  SHARING_PORT=10002
  BROADCAST_PORT=10003
end
