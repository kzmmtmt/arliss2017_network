# v0 (2017/8/22)
# KMLを作成

class KML
  def initialize(name)
    @header="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:kml=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n<Document>\n<name>#{name}</name>\n"
    @style="<Style id=\"sh_flag\">\n <IconStyle>\n<scale>1.4</scale>\n<Icon>\n<href>http://maps.google.com/mapfiles/kml/shapes/flag.png</href>\n</Icon>\n<hotSpot x=\"0.5\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>\n </IconStyle>\n </Style>\n <Style id=\"sh_R\">\n <IconStyle>\n <scale>1.3</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/paddle/R.png</href>\n </Icon>\n <hotSpot x=\"32\" y=\"1\" xunits=\"pixels\" yunits=\"pixels\"/>\n </IconStyle>\n <ListStyle>\n <ItemIcon>\n <href>http://maps.google.com/mapfiles/kml/paddle/R-lv.png</href>\n </ItemIcon>\n </ListStyle>\n </Style>\n <Style id=\"sh_ylw-pushpin\">\n <IconStyle>\n <scale>1.2</scale>\n </IconStyle>\n <LineStyle>\n <color>ff00ffff</color>\n <width>5</width>\n </LineStyle>\n </Style>\n <StyleMap id=\"msn_caution\">\n <Pair>\n <key>normal</key>\n <styleUrl>#sn_caution</styleUrl>\n </Pair>\n <Pair>\n <key>highlight</key>\n <styleUrl>#sh_caution</styleUrl>\n </Pair>\n </StyleMap>\n <StyleMap id=\"msn_ylw-pushpin\">\n <Pair>\n <key>normal</key>\n <styleUrl>#sn_ylw-pushpin</styleUrl>\n </Pair>\n <Pair>\n <key>highlight</key>\n <styleUrl>#sh_ylw-pushpin</styleUrl>\n </Pair>\n </StyleMap>\n <Style id=\"sh_cross-hairs_highlight\">\n <IconStyle>\n <scale>1.2</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/shapes/cross-hairs_highlight.png</href>\n </Icon>\n </IconStyle>\n </Style>\n <Style id=\"sn_L\">\n <IconStyle>\n <scale>1.1</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/paddle/L.png</href>\n </Icon>\n <hotSpot x=\"32\" y=\"1\" xunits=\"pixels\" yunits=\"pixels\"/>\n </IconStyle>\n <ListStyle>\n <ItemIcon>\n <href>http://maps.google.com/mapfiles/kml/paddle/L-lv.png</href>\n </ItemIcon>\n </ListStyle>\n </Style>\n <StyleMap id=\"msn_L\">\n <Pair>\n <key>normal</key>\n <styleUrl>#sn_L</styleUrl>\n </Pair>\n <Pair>\n <key>highlight</key>\n <styleUrl>#sh_L</styleUrl>\n </Pair>\n </StyleMap>\n <StyleMap id=\"msn_cross-hairs\">\n <Pair>\n <key>normal</key>\n <styleUrl>#sn_cross-hairs</styleUrl>\n </Pair>\n <Pair>\n <key>highlight</key>\n <styleUrl>#sh_cross-hairs_highlight</styleUrl>\n </Pair>\n </StyleMap>\n <StyleMap id=\"msn_R\">\n <Pair>\n <key>normal</key>\n <styleUrl>#sn_R</styleUrl>\n </Pair>\n <Pair>\n <key>highlight</key>\n <styleUrl>#sh_R</styleUrl>\n </Pair>\n </StyleMap>\n <StyleMap id=\"msn_flag\">\n <Pair>\n <key>normal</key>\n <styleUrl>#sn_flag</styleUrl>\n </Pair>\n <Pair>\n <key>highlight</key>\n <styleUrl>#sh_flag</styleUrl>\n </Pair>\n </StyleMap>\n <Style id=\"sh_poi\">\n <IconStyle>\n <scale>1.4</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/shapes/poi.png</href>\n </Icon>\n <hotSpot x=\"0.5\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>\n </IconStyle>\n </Style>\n <Style id=\"sn_poi\">\n <IconStyle>\n <scale>1.2</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/shapes/poi.png</href>\n </Icon>\n <hotSpot x=\"0.5\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>\n </IconStyle>\n </Style>\n <Style id=\"sn_flag\">\n <IconStyle>\n <scale>1.2</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/shapes/flag.png</href>\n </Icon>\n <hotSpot x=\"0.5\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>\n </IconStyle>\n </Style>\n <Style id=\"sn_ylw-pushpin\">\n <LineStyle>\n <color>ff00ffff</color>\n <width>5</width>\n </LineStyle>\n </Style>\n <Style id=\"sh_caution\">\n <IconStyle>\n <scale>1.4</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/shapes/caution.png</href>\n </Icon>\n <hotSpot x=\"0.5\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>\n </IconStyle>\n </Style>\n <StyleMap id=\"msn_poi\">\n <Pair>\n <key>normal</key>\n <styleUrl>#sn_poi</styleUrl>\n </Pair>\n <Pair>\n <key>highlight</key>\n <styleUrl>#sh_poi</styleUrl>\n </Pair>\n </StyleMap>\n <Style id=\"sh_L\">\n <IconStyle>\n <scale>1.3</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/paddle/L.png</href>\n </Icon>\n <hotSpot x=\"32\" y=\"1\" xunits=\"pixels\" yunits=\"pixels\"/>\n </IconStyle>\n <ListStyle>\n <ItemIcon>\n <href>http://maps.google.com/mapfiles/kml/paddle/L-lv.png</href>\n </ItemIcon>\n </ListStyle>\n </Style>\n <Style id=\"sn_R\">\n <IconStyle>\n <scale>1.1</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/paddle/R.png</href>\n </Icon>\n <hotSpot x=\"32\" y=\"1\" xunits=\"pixels\" yunits=\"pixels\"/>\n </IconStyle>\n <ListStyle>\n <ItemIcon>\n <href>http://maps.google.com/mapfiles/kml/paddle/R-lv.png</href>\n </ItemIcon>\n </ListStyle>\n </Style>\n <Style id=\"sn_caution\">\n <IconStyle>\n <scale>1.2</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/shapes/caution.png</href>\n </Icon>\n <hotSpot x=\"0.5\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>\n </IconStyle>\n </Style>\n <Style id=\"sn_cross-hairs\">\n <IconStyle>\n <scale>1.2</scale>\n <Icon>\n <href>http://maps.google.com/mapfiles/kml/shapes/cross-hairs.png</href>\n </Icon>\n </IconStyle>\n </Style>\n"
    @footer="</Document>\n</kml>\n"
    @line="<Placemark>\n<name>path</name>\n<description>path which cansat ran</description>\n<styleUrl>#msn_ylw-pushpin</styleUrl>\n<LineString>\n<extrude>1</extrude>\n<tessellate>1</tessellate>\n<altitudeMode>relativeToGround</altitudeMode>\n<coordinates>\n"
    @linefooter="</coordinates>\n</LineString>\n</Placemark>\n"
    @position=""
  end
  
  # 通ったルート追加
  def add_route(lat,lon)
    @line+="#{lon},#{lat},0 \n"
  end
  
  # 左折時のフラグ
  def add_left_flag(lat,lon,deg)
    add_position("LEFT","#{deg} deg","msn_L",lat,lon)
  end
  
  # 右折時のフラグ
  def add_right_flag(lat,lon,deg)
    add_position("RIGHT","#{deg} deg","msn_R",lat,lon)
  end
  
  # スタート地点
  def add_start_flag(lat,lon)
    add_position("START","mission started point","msn_cross-hairs",lat,lon)
  end
  
  # ブロック目標
  def add_target_flag(lat,lon,id)
    add_position("POINT#{id}","point #{id}","msn_flag",lat,lon)
  end
  
  # 終了地点
  def add_end_flag(lat,lon,message)
    add_position("#{message}","#{message}","msn_poi",lat,lon)
  end
  
  # スタック地点
  def add_stuck_flag(lat,lon)
    add_position("STUCK","STUCK","msn_caution",lat,lon)
  end
  
  # 場所にフラグ追加
  def add_position(name,discription,icon,lat,lon)
    @position+="<Placemark>\n<name>#{name}</name>\n<description>#{discription}</description>\n<styleUrl>##{icon}</styleUrl>\n<Point>\n<coordinates>#{lon},#{lat},0</coordinates>\n</Point>\n</Placemark>\n"
  end
  
  # 最後に出力(文字列として)
  def publish
    return @header+@style+@line+@linefooter+@position+@footer
  end
end
