# v0 (8/11)
# ブロックの探索が終了したことを通知
PATH=File::expand_path(File::dirname(__FILE__))
require "#{PATH}/tcp"
require "#{PATH}/parameters"

if ARGV.size==1 then
  sender=TcpSender.new(Parameter::SELF_MONITORING_PORT)
  id=ARGV[0].to_f.round
  if sender.send("127.0.0.1","explored #{id}") then
    puts("succeed to inform explored #{id}.")
    exit 0
  else
    puts("failed to inform")
    exit 1
  end
else
  puts("error: wrong argumment.")
  exit 1
end
