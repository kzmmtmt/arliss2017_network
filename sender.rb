# v0 (17/7/24)
# UDPでグローバルで送信するだけ

require "socket"

class UdpSender
  def initialize(port)
    @udp=UDPSocket::open
    @address=Socket::pack_sockaddr_in(port,"10.0.0.255") # ここのIPを変更
    @udp.setsockopt(Socket::SOL_SOCKET,Socket::SO_BROADCAST,1)
  end
  
  def send(msg)
    begin
      @udp.send(msg,0,@address)
      return true
    rescue Exception
      puts("Error: message is not sent.")
      return false
    end
  end
  
  def close
    @udp.close rescue nil
  end
end

# コマンドラインから実行されたときのみ呼ばれる
if(__FILE__==$0)
  if(ARGV.size!=3)
    puts("\aError: wrong arguments\nUsage: ruby #{__FILE__} port name msg")
  else
    sender=UdpSender.new(ARGV[0].to_i,ARGV[1])
    if(sender.send(ARGV[2]))
      puts("送信しました")
    end
    sender.close
  end
end

