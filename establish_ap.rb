# v0 (17/7/11)
def show_usage
  puts("Usage: sudo ruby #{__FILE__} \"SSID\" \"PASSWORD\"")
end

def write_file(path,text)
  result=system"sudo echo '#{text}' > #{path}"
  if(!result)
    puts("\aPermission denied! Run with sudo command.")
    show_usage
    exit(2)
  end
end

def establish_ap(ssid,password)
  text=<<-"EOS"
#for access point setting 6-9
interface=wlan0
#driver=nl80211
#driver=rtl871xdrv
#driver=rt2800usb
ssid=#{ssid}
hw_mode=g
channel=6
ieee80211n=1
wmm_enabled=1
#ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
macaddr_acl=0
#auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_passphrase=#{password}
rsn_pairwise=CCMP
  EOS

  write_file("/etc/hostapd/hostapd.conf",text)

  text=<<-'EOS'
# A sample configuration for dhcpcd.
# See dhcpcd.conf(5) for details.

# Allow users of this group to interact with dhcpcd via the control socket.
#controlgroup wheel

# Inform the DHCP server of our hostname for DDNS.
hostname

# Use the hardware address of the interface for the Client ID.
clientid
# or
# Use the same DUID + IAID as set in DHCPv6 for DHCPv4 ClientID as per RFC4361.
#duid

# Persist interface configuration when dhcpcd exits.
persistent

# Rapid commit support.
# Safe to enable by default because it requires the equivalent option set
# on the server to actually work.
option rapid_commit

# A list of options to request from the DHCP server.
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
# Most distributions have NTP support.
option ntp_servers
# Respect the network MTU.
# Some interface drivers reset when changing the MTU so disabled by default.
#option interface_mtu

# A ServerID is required by RFC2131.
require dhcp_server_identifier

# Generate Stable Private IPv6 Addresses instead of hardware based ones
slaac private

# A hook script is provided to lookup the hostname if not set by the DHCP
# server, but it should not be run by default.
nohook lookup-hostname

interface wlan0
static ip_address=10.0.0.1/24
  EOS

  write_file("/etc/dhcpcd.conf",text)

  text=<<-"EOS"
# Defaults for hostapd initscript
#
# See /usr/share/doc/hostapd/README.Debian for information about alternative
# methods of managing hostapd.
#
# Uncomment and set DAEMON_CONF to the absolute path of a hostapd configuration
# file and hostapd will be started during system boot. An example configuration
# file can be found at /usr/share/doc/hostapd/examples/hostapd.conf.gz
#
#DAEMON_CONF=""

# Additional daemon options to be appended to hostapd command:-
#       -d   show more debug messages (-dd for even more)
#       -K   include key data in debug messages
#       -t   include timestamps in some debug messages
#
# Note that -B (daemon mode) and -P (pidfile) options are automatically
# configured by the init.d script and must not be added to DAEMON_OPTS.
#
#DAEMON_OPTS=""

DAEMON_CONF="/etc/hostapd/hostapd.conf"
  EOS

  write_file("/etc/default/hostapd",text)

  text=<<-"EOS"
# interfaces(5) file used by ifup(8) and ifdown(8)

# Please note that this file is written to be used with dhcpcd
# For static IP, consult /etc/dhcpcd.conf and 'man dhcpcd.conf'

# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

iface eth0 inet manual

allow-hotplug wlan0
iface wlan0 inet manual
dns-nameservers 8.8.8.8
    #wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

#allow-hotplug wlan1
#iface wlan1 inet manual
    #wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
  EOS

  write_file("/etc/network/interfaces",text)

  system"sudo ifdown wlan0"
  result=`sudo ifup wlan0`
  if(result.include?("Failed")||result.include?("failed"))
    puts("\aError: can't connect to #{ssid}.\nPlease confirm SSID and PASSWORD.")
    exit(3)
  end
  if(result.include?("No DHCPOFFERS received"))
    puts("\aError: wrong PASSWORD")
    exit(4)
  end

  system"sudo service hostapd restart"
  system"sudo service isc-dhcp-server restart"
end


# コマンドラインから実行されたときのみ呼ばれる
if(__FILE__==$0)
  if(ARGV.size!=2)
    puts("\aError: wrong argument.")
    show_usage
    exit(1)
  end


  if(ARGV[1].size<8||ARGV[1].size>63)
    puts("\aError: PASSWORD length must be from 8 to 63 characters.")
  end

  ssid=ARGV[0]
  password=ARGV[1]

  establish_ap(ssid,password)
end

