# v0 (17/8/3)
# TCPで送受信するだけ

require "socket"

class TcpReceiver
  def initialize(port)
    @port=port
  end
  
  def receive
    begin
      @socket=TCPServer::open(@port)
      @s=@socket.accept
      while buf=@s.gets do
        str=buf
      end
      @s.close
      @socket.close
      return str
    rescue
      return nil
    end
  end
  
  def close
    @s.close rescue nil
    @socket.close rescue nil
  end
end


class TcpSender
  def initialize(port)
    @port=port
  end
  
  def send(ip,msg)
    begin
      @socket=TCPSocket::open(ip,@port)
      @socket.write(msg)
      @socket.close
      return true
    rescue
      return false
    end    
  end
  
  def close
    @socket.close rescue nil
  end
end
