# v1 (17/8/3)
# UDPで受信するだけ

require "socket"

class UdpReceiver

  def initialize(port,file)
    if(file!=nil) # ファイル書き込みを有効化
      @file=File::open(file,"w")
    else
      @file=nil
    end
    @socket=UDPSocket::open
    @socket.bind("",port)
  end
  
  # データの受信を開始
  def start_receiving
    @t=Thread::new do
      loop do
        begin
          str=@socket.recv(65535)
          if(@file!=nil)
            @file.puts str
            @file.flush
          else
            puts str
          end
        rescue Exception
          finish
        end
      end
    end
  end
  
  # データの受信を開始
  def start_receiving_single
    loop do
      str=@socket.recv(65535)
      if(@file!=nil)
        @file.puts str
      else
        puts str
      end
    end
  end
  
  
  # データの受信を開始
  def receive_once
    str=@socket.recv(65535)
    #puts("received: #{str}")
    return str
  end
  
  def close
    @socket.close rescue nil
  end
  
  def finish
    @t.kill rescue nil
    @socket.close rescue nil
    if(@file!=nil)
      @file.close
    end
    puts "終了しました"
  end
end

# コマンドラインから実行されたときのみ呼ばれる
if(__FILE__==$0)
  if(!(ARGV.size==2||ARGV.size==1))
    puts("\aError: wrong arguments\nUsage: ruby #{__FILE__} port [file_path]")
  else
    if(ARGV.size==2)
      file=ARGV[1]
    else
      file=nil
    end
    receiver=UdpReceiver.new(ARGV[0].to_i,file)
    begin
      receiver.start_receiving_single
    rescue Exception
      receiver.finish
    end
  end
end

