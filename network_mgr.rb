# v0 (17/7/11)
require "socket"

def get_ts1
  return Time.now.strftime("[%y/%m/%d-%H:%M:%S]")
end

# wi-fiのエントリー
class Entry
  def initialize(ip,name)
    @time=Time.now
    @ip=ip
    @name=name
  end
  
  def is_match?(ip,name)
    return ip==@ip&&name==@name
  end
  
  # 更新
  def update
    @time=Time.now
  end
  
  # 前回アップデートされてからの時間
  def uptime
    return Time.now-@time
  end
  
  # 出力用フォーマット
  def output
    return "#{@ip} #{@name}"
  end
end

# テーブル
class Table
  # ファイルのパスと有効時間とcleanを実行する間隔
  def initialize(path,available_time,check_interval)
    @list=[] # entryリスト
    @available_time=available_time
    @path=path
    refresh
    # 定期的に起動
    Thread::new do
      loop do
        sleep(check_interval)
        clean
      end
    end
  end
  
  # 受信した時に実行
  def receive(ip,name)
    @list.each do |item|
       # あればアップデート
      if(item.is_match?(ip,name))
        item.update
        return
      end
    end
    e=Entry.new(ip,name)
    @list.push(e)
    puts"#{get_ts1}connected: #{e.output}" if $DEBUG
    refresh
  end

  def count
    return @list.size
  end
  
  # ファイル書き込み
  def refresh
    file=File::open(@path,"w")
    @list.each do |item|
      file.puts(item.output)
    end
    file.close
  end

  def finish
    file=File::open(@path,"w")
    file.puts("null")
    file.close
  end
  
  # 掃除
  def clean
    flag=false
    @list.each_with_index do |item,index|
      if(item.uptime>@available_time)
        puts"#{get_ts1}disconnected: #{item.output}" if $DEBUG
        @list.delete_at(index)
        flag=true
      end
    end
    if(flag)
      refresh
    end
  end
end

# 送るだけ
class Sender
  def initialize(port,my_name)
    @port=port
    reset
    @name=my_name
  end
  
  def reset
    @udp=UDPSocket::open
    @address=Socket::pack_sockaddr_in(@port,"10.0.0.255")
    @udp.setsockopt(Socket::SOL_SOCKET,Socket::SO_BROADCAST,1)
  end
  
  # ブロードキャストに送信
  def send(msg)
    begin
      @udp.send(msg,0,@address)
    rescue
      @udp.close
      reset
      sleep(3)
      @udp.send(msg,0,@address)
    end
  end
  
  # 自分の情報を送信
  def send_info
    begin
      ip=Socket::getifaddrs.select {|x|
        x.name=="wlan0" and x.addr.ipv4?
      }.first.addr.ip_address
      send("#{ip} #{@name}\n")
    rescue Exception
      puts("#{get_ts1}ネットワークがありません")
    end
  end

  def finish
    @t.kill rescue nil
    @udp.close rescue nil
  end

  # 定期的にsendする
  def start_sending(interval)
    @t=Thread::new do
      loop do
        sleep(interval)
        send_info
      end
    end
  end
end


# 受け取るだけ
class Receiver
  def initialize(port)
    @port=port
    reset
  end

  def reset
    @socket=UDPSocket::open
    @socket.bind("",@port)
  end

  def finish
    @t.kill rescue nil
    @socket.close rescue nil
  end

  # データの受信を開始
  def start_receiving(table)
    @t=Thread::new do
      loop do
        begin
          str=@socket.recv(65535)
          buf=str.chomp.split(" ")
          if(buf.size==2)
            table.receive(buf[0],buf[1])
          end
        rescue
          @socket.close
          reset
        end
      end
    end
  end
end
