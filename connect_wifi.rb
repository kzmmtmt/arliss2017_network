# v0 (17/7/11)
def show_usage2
  puts("Usage: sudo ruby #{__FILE__} \"SSID\" \"PASSWORD\"")
end

def write_file2(path,text)
  result=system"sudo echo '#{text}' > #{path}"
  if(!result)
    puts("\aPermission denied! Run with sudo command.")
    show_usage2
    exit(2)
  end
end

def connect_wifi(ssid,passwd)
  text=<<-"EOS"
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=JP

network={
        ssid="#{ssid}"
        psk="#{passwd}"
        proto=RSN
        key_mgmt=WPA-PSK
        pairwise=CCMP
        auth_alg=OPEN
}
  EOS

  write_file2("/etc/wpa_supplicant/wpa_supplicant.conf",text)

  text=<<-'EOS'
# A sample configuration for dhcpcd.
# See dhcpcd.conf(5) for details.

# Allow users of this group to interact with dhcpcd via the control socket.
#controlgroup wheel

# Inform the DHCP server of our hostname for DDNS.
hostname

# Use the hardware address of the interface for the Client ID.
clientid
# or
# Use the same DUID + IAID as set in DHCPv6 for DHCPv4 ClientID as per RFC4361.
#duid

# Persist interface configuration when dhcpcd exits.
persistent

# Rapid commit support.
# Safe to enable by default because it requires the equivalent option set
# on the server to actually work.
option rapid_commit

# A list of options to request from the DHCP server.
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
# Most distributions have NTP support.
option ntp_servers
# Respect the network MTU.
# Some interface drivers reset when changing the MTU so disabled by default.
#option interface_mtu

# A ServerID is required by RFC2131.
require dhcp_server_identifier

# Generate Stable Private IPv6 Addresses instead of hardware based ones
slaac private

# A hook script is provided to lookup the hostname if not set by the DHCP
# server, but it should not be run by default.
nohook lookup-hostname
  EOS

  write_file2("/etc/dhcpcd.conf",text)

  text=<<-"EOS"
# Defaults for hostapd initscript
#
# See /usr/share/doc/hostapd/README.Debian for information about alternative
# methods of managing hostapd.
#
# Uncomment and set DAEMON_CONF to the absolute path of a hostapd configuration
# file and hostapd will be started during system boot. An example configuration
# file can be found at /usr/share/doc/hostapd/examples/hostapd.conf.gz
#
DAEMON_CONF="/etc/hostapd/hostapd.conf"

# Additional daemon options to be appended to hostapd command:-
# 	-d   show more debug messages (-dd for even more)
# 	-K   include key data in debug messages
# 	-t   include timestamps in some debug messages
#
# Note that -B (daemon mode) and -P (pidfile) options are automatically
# configured by the init.d script and must not be added to DAEMON_OPTS.
#
#DAEMON_OPTS=""
  EOS

  write_file2("/etc/default/hostapd",text)

  text=<<-"EOS"
# interfaces(5) file used by ifup(8) and ifdown(8)

# Please note that this file is written to be used with dhcpcd
# For static IP, consult /etc/dhcpcd.conf and 'man dhcpcd.conf'

# Include files from /etc/network/interfaces.d:
#source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

iface eth0 inet manual

auto wlan0
allow-hotplug wlan0
iface wlan0 inet dhcp
dns-nameservers 8.8.8.8

#pre-up iptables-restore < /etc/iptables.ipv4.nat


#comment out by chou 6-9 for access point setting
#iface wlan0 inet manual

wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

#added for access point setting
#iface wlan0 inet manual

# allow-hotplug wlan1
# iface wlan1 inet manual
#    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
  EOS

  write_file2("/etc/network/interfaces",text)

  system"sudo ifdown wlan0"
  result=`sudo ifup wlan0`
  if(result.include?("Failed")||result.include?("failed"))
    puts("\aError: can't connect to #{ssid}.\nPlease confirm SSID and PASSWORD.")
    exit(3)
  end
  if(result.include?("No DHCPOFFERS received"))
    puts("\aError: wrong PASSWORD")
    exit(4)
  end
end

# コマンドラインから実行されたときのみ呼ばれる
if(__FILE__==$0)
  if(ARGV.size!=2)
    puts("\aError: wrong argument.")
    show_usage2
    exit(1)
  end

  ssid=ARGV[0]
  passwd=ARGV[1]
  
  connect_wifi(ssid,passwd)
end
