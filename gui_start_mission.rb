# 探索を開始するプログラム
# ライブラリがあるか確認。なければインストール。
begin
  require "gtk3"
rescue Exception
  puts("GTKライブラリが見つかりませんでした。\nインストールします(このインストールは初回のみ発生します)。\nしばらくお待ちください…")
  if system("gem install gtk3") then
    puts("\nインストールが完了しました。\nプログラムを終了します。\n再度立ち上げてください。")
  else
    puts("\nインストールに失敗しました。\nインターネットに接続されているか, 権限が与えられているかなどを確認してください。")
  end
  exit(0)
end
P=File::expand_path(File::dirname(__FILE__))
require "#{P}/parameters"
require "#{P}/tcp"

def send_message(dialog,window,x,y)
  s=TcpSender::new(Parameter::ROUTE_PORT)
  puts("Sending position... (#{y},#{x})")
  if s.send("10.0.0.1","start #{x} #{y}") then
    puts("Succeeded to send position (#{y},#{x})")
    ok=Gtk::MessageDialog::new(parent:window,flags:Gtk::DialogFlags::MODAL,type:Gtk::MessageType::INFO,buttons:Gtk::ButtonsType::OK,message:"送信成功しました")
    ok.signal_connect("response") do |w,r|
      if r==Gtk::ResponseType::OK then
        Gtk::main_quit
      end
    end
    ok.run
  else
    puts("Failed to send position (#{y},#{x})")
    ng=Gtk::MessageDialog::new(parent:window,flags:Gtk::DialogFlags::MODAL,type:Gtk::MessageType::ERROR,buttons:Gtk::ButtonsType::CLOSE,message:"送信に失敗しました")
    ng.signal_connect("response") do |w,r|
      if r==Gtk::ResponseType::CLOSE then
        ng.destroy
        window.show_all
      end
    end
    ng.run
  end
  s.close
end

puts("プログラム起動中…")
window=Gtk::Window::new("マルチーズ")
window.border_width=10
window.window_position=Gtk::WindowPosition::CENTER
box=Gtk::Box::new(:vertical,10)
table=Gtk::Table::new(2,2,false)
table.set_row_spacings(5)
align=Gtk::Alignment.new(0,0,0,0)
label=Gtk::Label::new("探査領域の最も南西の座標を入力")
label.set_justify(Gtk::Justification::LEFT)
align.add(label)
box.pack_start(align,expand:false)
table.attach_defaults(Gtk::Label::new("緯度(北緯): "),0,1,0,1)
table.attach_defaults(Gtk::Label::new("経度(東経): "),0,1,1,2)
lat=Gtk::Entry::new # 緯度
lat.max_length=20
lon=Gtk::Entry::new # 経度
lon.max_length=20
table.attach_defaults(lat,1,2,0,1)
table.attach_defaults(lon,1,2,1,2)
box.pack_start(table,expand:true,fill:true,padding:5)
button=Gtk::Button::new(label:"OK")
button.margin_left=button.margin_right=20
box.pack_start(button,expand:true,fill:true)
window.add(box)

button.signal_connect("clicked") do
  begin
    y=Float(lat.text)
    x=Float(lon.text)
    d=Gtk::MessageDialog::new(parent:window,flags:Gtk::DialogFlags::MODAL,type:Gtk::MessageType::QUESTION,buttons:Gtk::ButtonsType::YES_NO,message:"#{y<0 ? "南" : "北"}緯#{y.abs}°, #{x<0 ? "西" : "東"}経#{x.abs}°に設定してもよろしいですか?")
    d.signal_connect("response") do |w,r|
      if r==Gtk::ResponseType::YES then
        send_message(d,window,x,y)
      end
    end
    d.run
    d.destroy
  rescue Exception
    d=Gtk::MessageDialog::new(parent:window,flags:Gtk::DialogFlags::MODAL,type:Gtk::MessageType::ERROR,buttons:Gtk::ButtonsType::CLOSE,message:"経度/緯度に正しい数値が入っていません")
    d.run
    d.destroy
  end
end

window.signal_connect("destroy") do
  puts("プログラムを終了します")
  Gtk::main_quit
end
window.show_all
Gtk::main

