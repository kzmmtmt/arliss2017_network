# v0 (17/8/13)
# motd
# ステータスを表示

require "#{File::expand_path(File::dirname(__FILE__))}/parameters"
require "socket"

puts("\n\033[1;3;4;33;40m Welcome to the Mulcheese Rover!! \033[0m\n\n")
puts("SSID: \033[1;37;45m #{Parameter::SSID_KEYWORD}_#{Parameter::MY_ID} \033[0m")
begin
  ip=Socket::getifaddrs.select{|x|
    x.name=="wlan0" and x.addr.ipv4?
  }.first.addr.ip_address
  puts("IP Address: \033[1;30;43m #{ip} \033[0m")
rescue Exception
  puts("Can't detect network.")
end
is_server=(`cat #{Parameter::PARENT_FLAG}`).chomp
if is_server=="true" then
  puts("Mode: \033[1;37;41m Server \033[0m")
else
  puts("Mode: \033[1;37;44m Client \033[0m")
end
puts("Date: #{Time.now}")
list=(`cat #{Parameter::TABLE_PATH}`).chomp
puts("\n[IP Tables]\n"+list)
puts("\n")
