# 通信を止める

# build networkのプロセスIDを探す
ps=`ps aux | grep "sudo ruby -d /home/pi/network/build_network.rb"`
begin
line=ps.split("\n")[0]
if(!line.include?("grep"))
  pid=line.split(" ")[1]
  system("sudo kill #{pid}")
  puts"プロセス#{pid}を停止しました"
else
  puts"build_networkは実行されていません"
end
rescue Exception
  puts"build_networkは実行されていません"
end
system"sudo service hostapd stop"
system"sudo ifdown wlan0"
puts"Wi-Fiを停波しました"
