# v1 (17/8/27)
# board3対応
# LED制御用
R_PIN=19
G_PIN=13
B_PIN=6
ECHO="sudo echo "
PATH_GPIO=" > /sys/class/gpio/"

# LED初期化(1回だけ呼ぶ)
def start_gpio
  pins=[R_PIN,G_PIN,B_PIN]
  pins.each do |p|
    system"#{ECHO}#{p}#{PATH_GPIO}export 2> /dev/null"
    system"#{ECHO}out#{PATH_GPIO}gpio#{p}/direction 2> /dev/null"
    system"#{ECHO}1#{PATH_GPIO}gpio#{p}/value 2> /dev/null"
  end
  light_up(0,0,0)
end

# 点灯
def light_up(r,g,b)
  pins=[R_PIN,G_PIN,B_PIN]
  signals=[r,g,b]
  for i in 0..2 do
    system "#{ECHO}#{signals[i]}#{PATH_GPIO}gpio#{pins[i]}/value 2> /dev/null"
  end
end

# コマンドラインから実行されたときのみ呼ばれる
if(__FILE__==$0)
  if(ARGV.size!=3)
    puts("\aError: wrong arguments\nUsage: sudo ruby #{__FILE__} R G B\nR,G,B={0,1}")
  else
    start_gpio
    light_up(ARGV[0],ARGV[1],ARGV[2])
  end
end
