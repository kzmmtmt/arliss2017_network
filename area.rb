# v0 (17/8/1)

require "#{File::expand_path(File::dirname(__FILE__))}/parameters"
require "#{File::expand_path(File::dirname(__FILE__))}/logger"

# 座標変換を行う
def convert_cordinate(point,east,north)
  p=`#{File::expand_path(File::dirname(__FILE__))}/files/dis_to_gps #{point.y} #{point.x} #{north} #{east}`
  buf=p.split(" ").map{|item|item.to_f}
  return Point.new(buf[1],buf[0])
end

# 距離
def distance(p1,p2)
  d=(`python #{File::expand_path(File::dirname(__FILE__))}/files/gps_to_dis.py "(#{p1.y},#{p1.x})" "(#{p2.y},#{p2.x})"`).to_f
  return d
end

# xが経度, yが緯度
class Point
  attr_accessor :x,:y
  def initialize(x,y)
    @x=x
    @y=y
  end
end


# ブロック
class Block
  attr_reader :center,:id
  def initialize(p,id) # 左上の座標
    @center=convert_cordinate(p,Parameter::BLOCK_WIDTH/2,Parameter::BLOCK_HEIGHT/2)
    @id=id
    @status=nil
    @by_me=nil
  end
  
  def is_explored?
    return @status==false||@status==true
  end
  
  def is_not_explored?
    return @status==nil
  end
  
  def is_discovered?
    return @status==true
  end

  # 探索済みへ(自分自身が探索した場合、trueへ)
  def explored(is_me)
    @status=false
    @by_me=is_me
  end
  
  def is_explored_by_me?
    return @by_me
  end

  # 発見
  def discovered
    @status=true
  end
end

# ブロックを集めた領域
class Area
  attr_accessor :target,:now

  def initialize(p) # 左上の座標
    @point=p
    @block=[]
    @now=nil
    @target=nil
    id=0
    for w in 0...Parameter::BLOCK_SIZE_HORIZONTAL do
      @block.push []
      for h in 0...Parameter::BLOCK_SIZE_VERTICAL do
        @block[w][h]=Block.new(convert_cordinate(p,w*Parameter::BLOCK_WIDTH,h*Parameter::BLOCK_HEIGHT),id)
        id+=1
      end
    end
  end

  # エリアIDを表示
  def show_area
    output("\n    N\n< W + E >\n    S")
    length=@block.last.last.id.to_s.size+1
    str="\n"
    for h in 1..Parameter::BLOCK_SIZE_VERTICAL do
      for w in 0...Parameter::BLOCK_SIZE_HORIZONTAL do
        str+=format("%*d",length,@block[w][Parameter::BLOCK_SIZE_VERTICAL-h].id)
      end
      str+="\n"
    end
    output(str)
    # 座標表示
    output("\n----cordinate----")
    for i in 0..@block.last.last.id do
      output(" #{i}: #{block_id(i).center.x},#{block_id(i).center.y}")
    end
    output("--------")
  end

  # ルートを表示
  def show_route(p)
    if @route==nil then
      return
    end
    output"\n(1) distant to start: #{distance(block_id(@route.first).center,p[0])}\n[1] route: #{@route}"
    if p.size==2 then
      output"(2) distant to start: #{distance(block_id(@route.last).center,p[1])}\n[2] route: #{@route.reverse}"
    end
  end
  
  def get_clients_route
    return (@route.reverse).join(",")
  end

  def set_route(r)
    @route=r
  end
  
  # 発見されたときの処理
  def discovered(id)
    block_id(id).discovered
    @target=id
    @now=nil
    # ファイル書き込み
    loop do
      begin
        f=File::open(Parameter::GOAL_LIST,"w")
        f.puts("0,0,-2")
        f.close
        break
      rescue
        output("Error: can't open file. retry in 1 sec.")
        sleep(1)
      end
    end
  end
  
  # 探索済みエリアのリストを返す
  def get_explored_block
    list=[]
    @block.flatten.each do |b|
      if b.is_explored? then
        list.push(b.id)
      end
    end
    str=""
    if @target!=nil then
      str=" #{@target}"
    end
    return list.join(",")+str
  end
  
  # 探索済みのエリアを受け取る
  def receive(list)
    flag=false
    list.each do |id|
      if block_id(id).is_not_explored? then
        flag=true
        block_id(id).explored(false)
      end
    end
    return flag
  end
  
  # 未探索エリアのルート生成
  def make_now_route
    list=[]
    @route.each do |r|
      # 未探索エリアなら
      if !(block_id(r).is_explored?) then
        list.push(block_id(r))
      end
    end
    @stack=list
    
    loop do
      begin
        f=File::open(Parameter::GOAL_LIST,"w")
        if list.size>0 then # 未探索エリアがあればリスト作成
          list.each do |b|
             # 経度,緯度,idの順
            f.puts("#{b.center.y},#{b.center.x},#{b.id}")
          end
        elsif @target==nil then # 探査終了(このエリアにはなし)
          f.puts("0,0,-1")
        else
          f.puts("#{block_id(@target).center.x},#{block_id(@target).center.y},#{block_id(@target).id}")
        end
        f.close
        break
      rescue
        output("Error: can't open file. retry in 1 sec.")
        sleep(1)
      end
    end
    output("remaining_block: #{list.size}\n->#{list.map{|item|item.id}}")
    
    if @stack.size==0 then # 探査終了
      return true
    else
      return false
    end
  end

  # ステータス表示
  # @: 自分の居る位置
  # #: 次の探索場所
  # -: 未探索
  # *: 探索済み(自分)
  # +: 探索済み(自分以外)
  # $:ターゲット
  def show_status(now=nil)
    @now=now if now!=nil
    cnt=0
    str="\n"
    for h in 0...Parameter::BLOCK_SIZE_VERTICAL do
      for w in 0...Parameter::BLOCK_SIZE_HORIZONTAL do
        hh=Parameter::BLOCK_SIZE_VERTICAL-h-1
        if @block[w][hh].id==@now then
          str+="@"
          cnt+=1
        elsif @block[w][hh].id==@target||@block[w][hh].is_discovered? then
          str+="$"
          cnt+=1
        elsif @block[w][hh].is_explored? then
          if @block[w][hh].is_explored_by_me? then
            str+="*"
          else
            str+="+"
          end
          cnt+=1
        elsif @block[w][hh].id==@stack.first.id then
          str+="#"
        elsif @block[w][hh].is_not_explored? then
          str+="-"
        end
      end
      str+="\n"
    end
    output(str)
    output"explored area: #{cnt}/#{Parameter::BLOCK_SIZE_VERTICAL*Parameter::BLOCK_SIZE_HORIZONTAL}"
  end

  def block_id(id)
    return @block.flatten.find{|item|item.id==id}
  end
  

  # ルート生成
  def generate_route(p) # pointの配列
    if p.size!=1&&p.size!=2 then
      return nil
    else
      list=[[],[],[],[]]
      for h in 0...Parameter::BLOCK_SIZE_VERTICAL do
        for w in 0...Parameter::BLOCK_SIZE_HORIZONTAL do
          if h%2==0 then
            list[0].push @block[w][h].id
            list[1].push @block[Parameter::BLOCK_SIZE_HORIZONTAL-w-1][h].id
          else
            list[0].push @block[Parameter::BLOCK_SIZE_HORIZONTAL-w-1][h].id
            list[1].push @block[w][h].id
          end
        end
      end
      for w in 0...Parameter::BLOCK_SIZE_HORIZONTAL do
        for h in 0...Parameter::BLOCK_SIZE_VERTICAL do
          if w%2==0 then
            list[2].push @block[w][h].id
            list[3].push @block[w][Parameter::BLOCK_SIZE_VERTICAL-h-1].id
          else
            list[2].push @block[w][Parameter::BLOCK_SIZE_VERTICAL-h-1].id
            list[3].push @block[w][h].id
          end
        end
      end
      # 裏返したやつも加える
      list+=list.map{|item|item.reverse}
      d=[]
      list.each do |r|
        if p.size==1 then # 1台
          d.push distance(block_id(r.first).center,p[0])
        elsif p.size==2 then # 2台
          d.push distance(block_id(r.first).center,p[0])+distance(block_id(r.last).center,p[1])
        end
      end
      @route=list[d.index(d.min)]
      show_route(p)
    end
  end
  
  # 探索済みにする
  def explored(id,is_me)
    block_id(id).explored(is_me)
    tmp=make_now_route
    show_status(id)
    @now=id
    return tmp
  end

end


