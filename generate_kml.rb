# v0 (2017/8/22)
# ファイルからKMLを作成

PATH=File::expand_path(File::dirname(__FILE__))
require "#{PATH}/kml"

def get_cordinate(line)
  return (((line.chomp.split("@"))[1]).split(",")).map{|item|item.to_f}
end

file=nil
filename=nil

# 引数にファイル名
if ARGV.size==1 then
  filename=ARGV[0]
  begin
    file=File::open(filename,"r")
  rescue
    puts("\aError: file #{filename} can't open")
    exit(1)
  end
else
  puts("\aError: wrong argument.")
  puts("USAGE: ruby #{__FILE__} \"FILENAME(log*.txt)\"")
  exit(1)
end

kml=KML.new(filename)

puts("loading file...")
file.each_line do |line|
  buf=line.chomp.split(" ")
  # NAVという文字が入っていなかったら次の行へ
  if buf[1]!="NAV" then
    next
  end
  
  lat,lon=get_cordinate(line)
  case buf[2]
  when "RIGHT" then
    kml.add_right_flag(lat,lon,buf[3].to_f)
    kml.add_route(lat,lon)
  when "LEFT" then
    kml.add_left_flag(lat,lon,buf[3].to_f)
    kml.add_route(lat,lon)
  when "START" then
    kml.add_start_flag(lat,lon)
    kml.add_route(lat,lon)
  when "SET" then # ゴールが更新
    kml.add_target_flag(lat,lon,buf[3].to_i)
  when "STUCK" then
    kml.add_stuck_flag(lat,lon)
    kml.add_route(lat,lon)
  when "DISCOVER" then # 発見
    kml.add_end_flag(lat,lon,"TARGET")
    kml.add_route(lat,lon)
  when "STOP" then # 探査終了
    kml.add_end_flag(lat,lon,"END")
    kml.add_route(lat,lon)
  end
end

file.close
file=File::open("#{filename}.kml","w")
file.puts(kml.publish)
puts("completed.")
file.close
