# v1 (17/8/27)
# 赤, 青のみ
# board3対応
# LED制御用
R_PIN=16
B_PIN=21
ECHO="sudo echo "
PATH_GPIO=" > /sys/class/gpio/"

# LED初期化(1回だけ呼ぶ)
def start_gpio
  pins=[R_PIN,B_PIN]
  pins.each do |p|
    system"#{ECHO}#{p}#{PATH_GPIO}export 2> /dev/null"
    system"#{ECHO}out#{PATH_GPIO}gpio#{p}/direction 2> /dev/null"
    system"#{ECHO}1#{PATH_GPIO}gpio#{p}/value 2> /dev/null"
  end
end

# 点灯
def light_up(r,g,b)
  pins=[R_PIN,B_PIN]
  signals=[r,b]
  for i in 0..1 do
    system "#{ECHO}#{signals[i]}#{PATH_GPIO}gpio#{pins[i]}/value 2> /dev/null"
  end
end

# コマンドラインから実行されたときのみ呼ばれる
if(__FILE__==$0)
  if(ARGV.size!=2)
    puts("\aError: wrong arguments\nUsage: sudo ruby #{__FILE__} R B\nR,B={0,1}")
  else
    start_gpio
    light_up(ARGV[0],0,ARGV[1])
  end
end
