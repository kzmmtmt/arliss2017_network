# v0 (17/8/13)
P=File::expand_path(File::dirname(__FILE__))
require "#{P}/parameters"
require "#{P}/explore"
require "#{P}/logger"

def get_time
  return Time.now.strftime("%y%m%d-%H%M")
end

# 経度, 緯度の順でもらう
if ARGV.size!=2 then
  puts("Error: wrong argument.")
  puts("usage: #{__FILE__} lon(x) lat(y)")
  puts("lon and lat must be current position.")
  exit(1)
end
now=Point.new(ARGV[0].to_f,ARGV[1].to_f) # 現在地
start=nil # 探索開始点
start=Point.new(Parameter::START_POINT[0],Parameter::START_POINT[1]) if Parameter::START_POINT.size==2

output("Mulcheese Mission Start!!")
# goal_listを空に(0,0,-3)
begin
  f=File::open(Parameter::GOAL_LIST,"w")
  f.puts("0,0,-3")
  f.close
rescue
  output("Can't reset goal_list.")
end

output("Now point: (#{now.x}, #{now.y})")
output("Start point: (#{start.x}, #{start.y})") if start!=nil
# 探査開始
exp=Explore.new
begin
  # 探査
  exp.start_explore(now,start)
rescue Exception=>e
  # 終了処理
  output("#{e.message}\n#{e.backtrace}")
  exp.finish
  output("\n\nFinished.")
end
