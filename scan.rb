# v0 (17/7/11)
# スキャンした結果、接続するべき端末があればそのSSID, なければnilを返す

def get_ts2
  return Time.now.strftime("[%y/%m/%d-%H:%M:%S]")
end

def scan(my_ssid,keyword)
  if($DEBUG)
    puts("#{get_ts2}scanning wi-fi...")
  end
  result=`sudo iwlist wlan0 scan | grep ESSID`
  ssid=[]
  result.each_line do |line|
    ssid.push ((line.chomp.split(":"))[1]).delete("\"")
  end
  ssid.push my_ssid # 自分のSSIDも追加
  if($DEBUG)
    puts("#{get_ts2} #{ssid.size} ssid detected.")
    ssid.each do |item|
      puts" * #{item}"
    end
  end

  target_ssid=[]
  number_list=[]
  ssid.each do |id|
    if(id.include?(keyword))
      buf=id.split('_')
      number=(buf.last).to_i
      number_list.push number
      target_ssid.push id
    end
  end
  
  idx=number_list.index(number_list.max) # 親になるべきindex
  if($DEBUG)
    puts("Related Wi-Fi: #{target_ssid.size}")
    target_ssid.each do |id|
      print"* #{id}"
      if(id==my_ssid)
        print" [SELF]"
      end
      if(target_ssid[idx]==id)
        print" [TARGET]"
      end
      puts""
    end
  end
  
  if(target_ssid.size==1)
    return nil
  elsif(target_ssid[idx]==my_ssid)
    return nil
  else # 最大のSSIDを返す
    return target_ssid[idx]
  end
end
