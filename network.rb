# v0 (17/8/8)
# 探索用(build_networkが動いている上で動く)
PATH=File::expand_path(File::dirname(__FILE__))
require "#{PATH}/parameters"
require "#{PATH}/tcp"
require "#{PATH}/logger"

# ネットワーク系(探査用プログラム)
class Network

  def initialize
    @sender=TcpSender.new(Parameter::ROUTE_PORT)
    @receiver=TcpReceiver.new(Parameter::ROUTE_PORT)
  end

  # 最新に更新
  def update
    begin
      f=File::open(Parameter::TABLE_PATH)
      @list=[]
      f.each_line do |line|
        buf=line.chomp!.split(" ")
        if buf.size==2 then
          @list.push [buf[0],buf[1]]
        end
      end
      f.close
      return true
    rescue
      return false
    end
  end

  # 自分のIPの取得
  def get_my_ip
    update
    @list.each do |item|
      if item[2]=="#{Parameter::SSID_KEYWORD}_#{Parameter::MY_ID}" then
        return item[1]
      end
    end
    return nil
  end

  # 自分以外のIP一覧
  def get_client_ip
    update
    tmp=[]
    @list.each do |item|
      if item[1]!="#{Parameter::SSID_KEYWORD}_#{Parameter::MY_ID}"&&item[1]!="#{Parameter::SSID_KEYWORD}_#{Parameter::TARGET_ID}" then
        tmp.push(item[0])
      end
    end
    return tmp
  end

  # 現在親かどうかを見分ける
  def is_parent?
    out=nil
    begin
      File::open(Parameter::PARENT_FLAG) do |f|
        str=f.read.chomp!
        if(str=="true")
          out=true
        elsif(str=="false")
          out=false
        end
      end
    rescue
      output"File IO Error. Can't read #{Parameter::PARENT_FLAG}."
    end
    return out
  end
  
  # ロスト機の電波をキャッチしているか
  def detect_lost_rover?
    update
    @list.each do |item|
      if item[1]=="#{Parameter::SSID_KEYWORD}_#{Parameter::TARGET_ID}" then
        return true
      end
    end
    return false
  end

  # 接続数(自分以外)確認
=begin
  def get_client_num
    cnt=0
    begin
      File::open(Parameter::TABLE_PATH) do |f|
        f.each_line do |line|
          cnt+=1
        end
      end
    rescue
      output"File IO Error. Can't read #{Parameter::TABLE_PATH}."
      return nil
    end
    return cnt-1
  end
=end
  
  # もう一方に送信
  def send(msg)
    if get_client_ip.size==1 then
      return @sender.send(get_client_ip[0],msg)
    end
    return false
  end
  
  # 相手に送れるまで送り続ける
  def send_forever(msg)
    loop do
      tmp=get_client_ip
      if tmp.size==1 then
        if @sender.send(get_client_ip[0],msg)==true then
          break
        end
      else
        output("Error: clients are too many or 0.\n ->#{tmp}\nRetry in 5 sec.")
      end
      sleep 5
    end
  end
  
  # 受信
  def receive_forever
    loop do
      msg=@receiver.receive
      if msg!=nil then
        return msg.chomp
      end
    end
  end
  
  def finish
    @sender.close rescue nil
    @receiver.close rescue nil
  end
end
