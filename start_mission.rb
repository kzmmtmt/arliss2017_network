# 送信するだけ
# TCPで送受信するだけ

P=File::expand_path(File::dirname(__FILE__))
require "#{P}/parameters"
require "#{P}/tcp"

# 経度, 緯度の順でもらう
if ARGV.size!=2 then
  puts("Error: wrong argument.")
  puts("usage: #{__FILE__} lon(x) lat(y)")
  puts("lon and lat must be start position.")
  exit(1)
end
s=TcpSender::new(Parameter::ROUTE_PORT)
puts("Sending position... (#{ARGV[0]},#{ARGV[1]})")
if s.send("10.0.0.1","start #{ARGV[0]} #{ARGV[1]}") then
  puts("Succeeded to send position (#{ARGV[0]},#{ARGV[1]})")
else
  puts("Failed to send position (#{ARGV[0]},#{ARGV[1]})")
end
puts("Finish program.")
