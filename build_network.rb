# v1 (17/7/16)
PATH=File::expand_path(File::dirname(__FILE__))
require "#{PATH}/parameters"
require "#{PATH}/establish_ap"
require "#{PATH}/connect_wifi"
require "#{PATH}/scan"
require "#{PATH}/network_mgr"
require "#{PATH}/led2"

def get_time
  return Time.now.strftime("%y%m%d-%H%M")
end

def get_ts
  return Time.now.strftime("[%y/%m/%d-%H:%M:%S]")
end

def write_parent_flag(flag)
  f=File::open(Parameter::PARENT_FLAG,"w")
  f.puts flag.to_s
  f.close
end

if($DEBUG&&Parameter::LOG)
  stdout=STDOUT.dup
  STDOUT.reopen("#{Parameter::LOG_PATH}/network-#{get_time}.log","w")
end

# start program
target=nil # 現在の接続先

MY_SSID="#{Parameter::SSID_KEYWORD}_#{Parameter::MY_ID}"
if($DEBUG)
  puts"#{get_ts}My SSID: #{MY_SSID}"
end

start_gpio
# 親になる
establish_ap(MY_SSID,Parameter::PASSWORD)
write_parent_flag(true)
light_up(1,0,0)


# 自分のIPを送受信する
table=Table.new(Parameter::TABLE_PATH,Parameter::AVAILABLE_TIME,Parameter::CLEAN_INTERVAL)
sender=Sender.new(Parameter::PORT,MY_SSID)
receiver=Receiver.new(Parameter::PORT)
sender.start_sending(Parameter::SEND_INTERVAL)
receiver.start_receiving(table)

begin
  loop do
    sleep(Parameter::CHECK_INTERVAL) # 待つ
    tmp=scan(MY_SSID,Parameter::SSID_KEYWORD) # スキャンの結果
    if(tmp!=target) # 変化した場合
      target=tmp;
      if(target) # 接続
        puts"#{get_ts}connecting to #{target}" if $DEBUG
        connect_wifi(target,Parameter::PASSWORD)
        light_up(0,0,1)
        write_parent_flag(false)
      else # 親になる
        puts"#{get_ts}establishing AP (#{MY_SSID})" if $DEBUG
        establish_ap(MY_SSID,Parameter::PASSWORD)
        light_up(1,0,0)
        write_parent_flag(true)
      end
    end
    # 誰かに接続されていれば色を紫色に
    if(target==nil)
      if(table.count>1)
        light_up(1,0,1)
      else
        light_up(1,0,0)
      end
    end
  end
rescue Exception # 強制停止時
  puts"#{get_ts}プログラムを終了します"
  if($DEBUG&&Parameter::LOG)
    STDOUT.flush
    STDOUT.close
    $stdout=stdout
  end
  puts"プログラムの終了処理をしています。お待ちください。"
  table.finish
  sender.finish
  receiver.finish
  light_up(0,0,0)
  write_parent_flag(nil)
  puts"ネットワークビルダーは安全に終了しました"
  exit(0)
end
