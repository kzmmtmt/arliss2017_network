# v0 (17/8/13)
# ログ保存用
require "#{File::expand_path(File::dirname(__FILE__))}/parameters"
require "#{File::expand_path(File::dirname(__FILE__))}/sender"


def get_ts
  return Time.now.strftime("[%y/%m/%d-%H:%M:%S] ")
end

def get_time
  return Time.now.strftime("%y%m%d-%H%M")
end

FILE="#{Parameter::MISSION_LOG}mission-#{get_time}.log"
SENDER=UdpSender.new(Parameter::BROADCAST_PORT)


def output(str)
  puts(str)
  system("echo \'#{get_ts}#{str}\' >> #{FILE}") rescue puts("Can't open log file.")
  SENDER.send(str+"\n")
end
