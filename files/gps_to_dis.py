# -*- coding:utf-8 -*-
from math import radians, cos, sin, asin, sqrt  
import sys
  
def haversine(gps1, gps2): # 经度1，纬度1，经度2，纬度2 （十进制度数）  
    """ 
    Calculate the great circle distance between two points  
    on the earth (specified in decimal degrees) 
    """  
    lon1, lat1 = gps1
    lon2, lat2 = gps2
    # 将十进制度数转化为弧度  
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])  
  
    # haversine公式  
    dlon = lon2 - lon1   
    dlat = lat2 - lat1   
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2  
    c = 2 * asin(sqrt(a))   
    r = 6371 # 地球平均半径，单位为公里  
    return c * r * 1000  
	
if __name__ == '__main__':
    gps1 = eval( sys.argv[1] )
    gps2 = eval( sys.argv[2] )
    print haversine(gps1, gps2)

# usage
# python gps_to_dis.py "(xx.xxx,xx.xxx)" "(xx.xxx,xx.xxx)"
