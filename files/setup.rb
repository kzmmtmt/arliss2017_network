# セットアップスクリプト v1.1 (17/8/7)
# ホームディレクトリで実行
# sudoコマンドで実行

# インストールするソフトウェア
softwares=["isc-dhcp-server","hostapd","vim","tmux","gpsd","gpsd-clients","libgps-dev"]

def show_usage
  puts("Usage(@/home/pi): sudo ruby #{__FILE__} ID")
  puts("ID is the integer number of SSID.")
end

def write_file(path,text)
  result=system"sudo echo '#{text}' > #{path}"
  if(!result)
    puts("\aPermission denied! Run with sudo command.")
    show_usage
    exit(1)
  end
end

if ARGV.size==0 then
  puts("wrong argment. Please input ID of SSID.")
  show_usage
  exit(1)
end

system("echo \"#{ARGV[0]}\" > /home/pi/id")
puts("IDは#{ARGV[0]}に設定されました")

# 要らないフォルダの削除
system"sudo rm -rf Documents Downloads Music Pictures Public python_games Templates Videos"

# リストの更新
system"cd /home/pi/"
puts"ソフトウェアリストを更新しています…"
result=system"sudo apt-get -y update"
if(!result)
  puts("Error: no internet or permission denied!")
  show_usage
  exit(1)
end
system"sudo apt-get -y upgrade"


# ソフトウェアのインストール
puts"ソフトウェアをインストールしています…"
softwares.each do |item|
  system"sudo apt-get -y install #{item}"
end

# git 設定
system"git config --global user.name \"mulcheese_developer\""
system"git config --global user.email \"takadamalab@gmail.com\""
puts"gitリポジトリをダウンロードしています…"

# git clone
system"git clone https://kzmmtmt@bitbucket.org/kzmmtmt/arliss2017_network.git network"
system"sudo chmod 777 network"
system"sudo chmod 777 network/*"
system"git clone https://github.com/takadamalab2017mainprogram/rovermain.git"
system"sudo chmod 777 rovermain"
system"sudo chmod 777 rovermain/*"


# 設定ファイル書き換え
puts"設定ファイルを書き換えています…"

# bashrcにmotdをを追加
system("echo \'ruby /home/pi/network/motd.rb\' >> /home/pi/.bashrc")

# vimrc
text=<<-'EOS'
set nu "行番号を表示する"
set nuw=1 "行番号の最小幅"
set wmnu "補完候補を表示する"
set list "不可視文字の表示する"
set lcs=tab:>-,trail:.,eol:$,extends:>,precedes:< "不可視文字の表示"
set wrap "折り返し表示を有効にする"
set cul "カーソル行の強調表示"
set sm "対応する括弧を強調表示する"
set spell "スペルチェックを有効にする"
sy on "シンタックスハイライトを有効にする"
set so=3 "カーソル上下に確保する行数"
set bg=dark "ハイライトカラーテーマ"
set ai "前の行に合わせた自動インデントを行う"
set si "文脈に合わせた高度な自動インデントを行う"
set cin "K&Rスタイルのインデントを行う"
set sta "行頭でのTabでswの値の分だけインデントする"
set et "Tabの代わりにスペース使う"
filetype plugin indent on "ファイルタイプ検出とプラグインを有効にする"
set ts=2 "タブの表示幅"
set sts=2 "タブ幅"
set sw=2 "自動インデントの幅"
set smd  "ステータスラインに現在モードを表示する"
set sc  "コマンドをステータスラインに表示する"
set ru  "カーソルの座標を表示する"
set ls=2 "ステータスラインを常に表示する"
set ch=1 "コマンドラインの行数"
set statusline=%F%m%r%h%w\ [FOR:%{&ff}][TYP:%Y][ENC:%{&fileencoding}][ASC:\%03.3b][%l,%v][%L:%p%%] "ステータスラインの内容"
set ic "検索時に大小文字の区別をしない"
set scs "パターンに大文字を含む時は大小文字を区別する"
set is "インクリメンタルサーチを行う"
set ws "末尾まで行ったら先頭から再検索する"
set hls "検索結果をハイライト"
set eb "エラー時にエラー音を鳴らす"
set novb "画面を点滅させない"
set nocp "Vi互換モードにしない"
set cb=unnamed,autoselect "クリップボードの設定"
set fenc=utf-8 "ファイル文字コード"
set enc=utf-8 "デフォルトの文字コード"
set bs=indent,eol,start "バックスペースを押した時の振舞"
set wim=list:full "wcの補完モード"
set shm+=I "ウガンダを表示しない"
EOS
write_file("/home/pi/.vimrc",text)
system"sudo cp /home/pi/.vimrc /home/pi/vimrc"
system"sudo mv /home/pi/vimrc /etc/vim/"

# rc.local
text=<<-'EOS'
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

# Print the IP address
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
fi

sudo /home/pi/startup.sh

exit 0
EOS
write_file("/etc/rc.local",text)

## 起動スクリプト
text=<<-'EOS'
#!/bin/bash
sudo ruby -d /home/pi/network/build_network.rb &
EOS
write_file("/home/pi/startup.sh",text)
system"sudo chmod 777 /home/pi/startup.sh"

## dhcpd.conf
text=<<-'EOS'
#
# Sample configuration file for ISC dhcpd for Debian
#
#

# The ddns-updates-style parameter controls whether or not the server will
# attempt to do a DNS update when a lease is confirmed. We default to the
# behavior of the version 2 packages ("none", since DHCP v2 did not
# have support for DDNS.)
ddns-update-style none;

# option definitions common to all supported networks...
#option domain-name "example.org";
#option domain-name-servers ns1.example.org, ns2.example.org;

default-lease-time 600;
max-lease-time 7200;

# If this DHCP server is the official DHCP server for the local
# network, the authoritative directive should be uncommented.
authoritative;

# Use this to send dhcp log messages to a different log file (you also
# have to hack syslog.conf to complete the redirection).
log-facility local7;

# No service will be given on this subnet, but declaring it helps the 
# DHCP server to understand the network topology.

#subnet 192.168.42.0  netmask 255.255.255.0 {
#    range 192.168.42.0 192.168.42.50;
#    option broadcast-address 192.168.42.255;
#    option routers 192.168.42.1;
#    default-lease-time 600;
#    max-lease-time 7200;
#    option domain-name "local";
#    option domain-name-servers 8.8.8.8,8.8.4.4;
#}

subnet 10.0.0.0  netmask 255.255.255.0 {
    range 10.0.0.2 10.0.0.254;
    option broadcast-address 10.0.0.255;
    option routers 10.0.0.1;
    default-lease-time 600;
    max-lease-time 7200;
    option domain-name "local";
    option domain-name-servers 8.8.8.8,8.8.4.4;
}
# This is a very basic subnet declaration.

#subnet 10.254.239.0 netmask 255.255.255.224 {
#  range 10.254.239.10 10.254.239.20;
#  option routers rtr-239-0-1.example.org, rtr-239-0-2.example.org;
#}

# This declaration allows BOOTP clients to get dynamic addresses,
# which we do not really recommend.

#subnet 10.254.239.32 netmask 255.255.255.224 {
#  range dynamic-bootp 10.254.239.40 10.254.239.60;
#  option broadcast-address 10.254.239.31;
#  option routers rtr-239-32-1.example.org;
#}

# A slightly different configuration for an internal subnet.
#subnet 10.5.5.0 netmask 255.255.255.224 {
#  range 10.5.5.26 10.5.5.30;
#  option domain-name-servers ns1.internal.example.org;
#  option domain-name "internal.example.org";
#  option routers 10.5.5.1;
#  option broadcast-address 10.5.5.31;
#  default-lease-time 600;
#  max-lease-time 7200;
#}

# Hosts which require special configuration options can be listed in
# host statements.   If no address is specified, the address will be
# allocated dynamically (if possible), but the host-specific information
# will still come from the host declaration.

#host passacaglia {
#  hardware ethernet 0:0:c0:5d:bd:95;
#  filename "vmunix.passacaglia";
#  server-name "toccata.fugue.com";
#}

# Fixed IP addresses can also be specified for hosts.   These addresses
# should not also be listed as being available for dynamic assignment.
# Hosts for which fixed IP addresses have been specified can boot using
# BOOTP or DHCP.   Hosts for which no fixed address is specified can only
# be booted with DHCP, unless there is an address range on the subnet
# to which a BOOTP client is connected which has the dynamic-bootp flag
# set.
#host fantasia {
#  hardware ethernet 08:00:07:26:c0:a5;
#  fixed-address fantasia.fugue.com;
#}

# You can declare a class of clients and then do address allocation
# based on that.   The example below shows a case where all clients
# in a certain class get addresses on the 10.17.224/24 subnet, and all
# other clients get addresses on the 10.0.29/24 subnet.

#class "foo" {
#  match if substring (option vendor-class-identifier, 0, 4) = "SUNW";
#}

#shared-network 224-29 {
#  subnet 10.17.224.0 netmask 255.255.255.0 {
#    option routers rtr-224.example.org;
#  }
#  subnet 10.0.29.0 netmask 255.255.255.0 {
#    option routers rtr-29.example.org;
#  }
#  pool {
#    allow members of "foo";
#    range 10.17.224.10 10.17.224.250;
#  }
#  pool {
#    deny members of "foo";
#    range 10.0.29.10 10.0.29.230;
#  }
#}
EOS
write_file("/etc/dhcp/dhcpd.conf",text)

## gpsd
text=<<-'EOS'
# Default settings for the gpsd init script and the hotplug wrapper.

# Start the gpsd daemon automatically at boot time
START_DAEMON="true"

# Use USB hotplugging to add new USB devices automatically to the daemon
USBAUTO="true"

# Devices gpsd should collect to at boot time.
# They need to be read/writeable, either by user gpsd or the group dialout.
DEVICES="/dev/ttyAMA0"

# Other options you want to pass to gpsd
GPSD_OPTIONS="/dev/ttyAMA0"

EOS
write_file("/etc/default/gpsd",text)

puts"\ncompleted!!\nキーボード設定及びSSHの有効化, シリアルの許可はsudo raspi-configで変更してください\n必ずnetwork/parameters.rbのIDを変更してから再起動してください"
