#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gps_ch2.h"
#include <string>

//目標緯度の計算
double Gps_ch::ido()
{
	M = a * (1 - e_2) / pow(W, 3.0);
	//printf("M=%.6f\n",M);
	//緯度差
	if ((goal_dir_r >= 0 && goal_dir_r <= M_PI_2)
	    ||(goal_dir_r >= M_PI && goal_dir_r <= 3 * M_PI_2))
	  {
	    ido_sa = goal_d*sin(goal_dir_r) / M;
	  }
	//else if(goal_dir_r >= M_PI && goal_dir_r <= 3 * M_PI_2)
	//{
	//  ido_sa = goal_d*cos(goal_dir_r) / M;
	//}
	else if ((goal_dir_r > M_PI_2 && goal_dir_r < M_PI)
		 || (goal_dir_r > 3 * M_PI_2 && goal_dir_r < 2 * M_PI))
	  {
	    ido_sa = goal_d*sin(goal_dir_r) / M;
	  }
	/*else if(goal_dir_r > 3 * M_PI_2 && goal_dir_r < 2 * M_PI)
	  {
	    ido_sa = goal_d*sin(goal_dir_r) / M;
	  }
	*/
	goal_ido = cur_ido + ido_sa*(180 / M_PI);
	return goal_ido;
}
//目標経度の計算
double Gps_ch::keido()
{
	N = a / W;
	//経度差
	if ((goal_dir_r >= 0 && goal_dir_r <= M_PI_2)
	    ||(goal_dir_r >= M_PI && goal_dir_r <= 3 * M_PI_2))
	  {
	    keido_sa = goal_d*cos(goal_dir_r) / (N * cos(i));
	  }
	//else if(goal_dir_r >= M_PI && goal_dir_r <= 3 * M_PI_2)
	  //{
	  //  keido_sa = goal_d*sin(goal_dir_r) / (N * cos(i));
	//}
	else if ((goal_dir_r > M_PI_2 && goal_dir_r < M_PI)
		 || (goal_dir_r > 3 * M_PI_2 && goal_dir_r < 2 * M_PI))
	  {
	    keido_sa = goal_d*cos(goal_dir_r) / (N * cos(i));
	  }
	/*
	else if(goal_dir_r > 3 * M_PI_2 && goal_dir_r < 2 * M_PI)
	  {
	    keido_sa = goal_d*cos(goal_dir_r) / (N * cos(i));
	  }
	*/ 
	goal_keido = cur_keido + keido_sa * (180 / M_PI);
	return goal_keido;
}

//緯度経度を求める共通の計算
void Gps_ch::AllcalT()
{
	//本来cur_idoの部分には平均の緯度が入るが平均を求めずそのまま入力している
	WT = sqrt(1 - e_2 * pow(sin(cur_ido * M_PI / 180), 2.0));
	MT = a * (1 - e_2) / pow(WT, 3.0);
	//現在地の緯度を平均の緯度と置いて計算した緯度差
	//平均緯度も現在地の緯度も近ければほとんど誤差は無いことを利用している
	if ((goal_dir_r >= 0 && goal_dir_r <= M_PI_2)
		|| (goal_dir_r >= M_PI && goal_dir_r <= 3 * M_PI_2))
	{
		diT = goal_d * cos(goal_dir_r) / MT;
		
	}
	else if ((goal_dir_r > M_PI_2 && goal_dir_r < M_PI)
		|| (goal_dir_r > 3 * M_PI_2 && goal_dir_r < 2 * M_PI))
	{
		diT = - goal_d * cos(goal_dir_r) / MT;
	}
	
	//printf("Approximation ido=%f\n", diT*180/M_PI);
	//平均緯度(ラジアン)
	i = cur_ido * (M_PI / 180) + diT / 2;
	//printf("average ido=%f\n", i*180/M_PI);
	W = sqrt(1 - e_2*pow(sin(i), 2.0));
	
}

int main(int argc, char *argv[])
{
	Gps_ch gc;
	//コマンドラインから緯度取得
	gc.cur_ido = atof(argv[1]);
	//コマンドラインから経度取得
	gc.cur_keido = atof(argv[2]);
	//コマンドラインから北へ進む距離取得
	gc.ch_north = atof(argv[3]);
	//コマンドラインから東へ進む距離取得
	gc.ch_east = atof(argv[4]);
	//目標点までの距離(m)
	gc.goal_d = sqrt(pow(atof(argv[3]), 2.0) + pow(atof(argv[4]), 2.0));
	
	
	
	//printf("コマンドライン引数は%d個\n", argc);
	//目標経度を表示
	/*
	printf("-First-\n");
	printf("Current ido=%f\n", gc.cur_ido);
	printf("Current keido=%f\n", gc.cur_keido);
	printf("North distanse=%f(m)\n", atof(argv[3]));
	printf("East distance=%f(m)\n", atof(argv[4]));
	printf("Distanse=%f(m)\n", gc.goal_d);
	*/
	//方角radianを求めるatan2の範囲は-pi~piであることに注意
	if (gc.ch_north >= 0)
	{
		//第1,2象限の時は普通に計算
		gc.goal_dir_r = atan2(gc.ch_north, gc.ch_east);
	}
	else if (gc.ch_north < 0)
	{
		//第3,4象限の時はatanの範囲的に2piを足す
		gc.goal_dir_r = atan2(gc.ch_north, gc.ch_east) + M_PI * 2.0;
	}
	/*
	printf("Direction=%f(rad)\n", gc.goal_dir_r);
	printf("\n");
	printf("-Calculate-\n");
	*/
	//必要な計算を行う
	gc.AllcalT();
	/*
	printf("\n");
	printf("-Result-\n");
	printf("Target ido:%f\n", gc.ido());
	printf("Target keido:%f\n", gc.keido());
	printf("\n");
	printf("-Confirm-\n");
	*/
	printf("%f %f\n",gc.ido(),gc.keido());
	/*
	if (gc.ch_north > 0)
	{
		printf("Want to go to NORTH...\n");
		if (gc.goal_ido - gc.cur_ido > 0)
		{
			printf("I go to NORTH! OK!\n");
		}
		else if (gc.goal_ido - gc.cur_ido < 0)
		{
			printf("I go to SOUTH FALDE\n");
		}
	}
	else if (gc.ch_north < 0)
	{
		printf("Want to go to SOUTH...\n");
		if (gc.goal_ido - gc.cur_ido > 0)
		{
			printf("I go to NORTH FALDE\n");
		}
		else if (gc.goal_ido - gc.cur_ido < 0)
		{
			printf("I go to SOUTH! OK!\n");
		}
	}
	printf("\n");
	if (gc.ch_east > 0)
	{
		printf("Want to go to EAST...\n");
		if (gc.goal_keido - gc.cur_keido > 0)
		{
			printf("I go to EAST! OK!\n");
		}
		else if (gc.goal_keido - gc.cur_keido < 0)
		{
			printf("I go to WEST FALDE\n");
		}
	}
	else if (gc.ch_east < 0)
	{
		printf("Want to go to WEST...\n");
		if (gc.goal_keido - gc.cur_keido > 0)
		{
			printf("I go to EAST FALDE\n");
		}
		else if (gc.goal_keido - gc.cur_keido < 0)
		{
			printf("I go to WEST! OK!\n");
		}
	}
	*/
	return 0;
}
