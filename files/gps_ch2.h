#include <string.h>
#define e_2  0.006694379990197588
        //赤道半径
#define a  6378137.000
#define b 6356752.314140
class Gps_ch
{
	/*ヒュベニの公式を用いる
	d=sqrt{(d_yM)^2+(d_xcosμ_y)^2}
	d:距離
	d_y:緯度の差(rad)
	d_x:経度の差(rad)
	μ_y:緯度の平均値(rad)
	M:子午線曲率半径
	N:なんとか線曲率半径
	W:名前無し
	定数
	e:第一離心率
	a:赤道半径
	b:極半径
	式がかなり多い
	*/
public:
	/*
	コマンドライン引数から取る値
	*/
	//変換前のGPSの座標の経度(10進法,実数)
	double cur_keido;
	//変換前のGPSの座標の緯度(10進法, 実数)
	double cur_ido;
	//変換先の東への距離(m)
	double ch_east;
	//変換先の北への距離(m)
	double ch_north;

	/*
	コマンドライン引数を基に計算する値
	*/
	//目標点までの距離(m)
	double goal_d;
	//目標点の方位
	double goal_dir_r;
	//目標緯度
	double goal_ido;
	//目標経度
	double goal_keido;
	//2点の緯度の差
	double ido_sa;
	//2点の緯度の差をラジアンにしたもの
	double ido_sa_r;
	//2点の経度の差
	double keido_sa;
	//2点の経度の差をラジアンにしたもの
	double keido_sa_r;
	//方角
	char* direction;

	//求めたい値iを求める上で必要な値
	double MT;
	double NT;
	double WT;
	double diT;
	double i;
	//子午線曲率半径
	double M;
	//卯酉線曲率半径
	double N;
	//子午線・卯酉線曲線率半径の分母
	float W;
	//iを求めるための前提計算
	void AllcalT();

	//目的の緯度を求める
	double ido();
	double keido();
	
	/*GRS80(世界測地系)を使用*/
	//離心率を2乗したもの
	//double e_2 = 0.006694379990197588;
	//赤道半径
	//double a = 6378137.000;
	//極半径
	//double b = 6356752.314140;

	//コンストラクタ
	Gps_ch()
	{}
	//デストラクタ
	~Gps_ch()
	{}
};
