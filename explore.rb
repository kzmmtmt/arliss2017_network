# v0 (17/8/11)
# 探索用(build_networkが動いている上で動く)
PATH2=File::expand_path(File::dirname(__FILE__))
require "#{PATH2}/parameters"
require "#{PATH2}/network"
require "#{PATH2}/area"
require "#{PATH2}/tcp"
require "#{PATH2}/sender"
require "#{PATH2}/receiver"
require "#{PATH2}/logger"

class Explore

  def initialize
    @network=Network.new
    @finished_flag=false # 終了フラグ
    @flag1=@flag2=@flag3=false
  end

  # ポジションをパース
  def parse_pos(key,str)
    data=(str.chomp).split(" ")
    if data[0]!=key||data.size!=3 then
      return nil
    end
    return Point.new(data[1].to_f,data[2].to_f)
  end

  # もう1台のposition取得(親)
  # 要:通信切断対策
  def get_position
    # "pos(ポジションをください)"というmsgを送る
    output("Detected #{@network.get_client_ip}")
    @network.send_forever("pos")
    output("Call position")
    
    # positionを受け取る
    pos=nil
    loop do 
      pos=parse_pos("pos",@network.receive_forever)
      if pos!=nil then
        output("Received position (#{pos.x},#{pos.y})")
        break
      end
    end
    return pos
  end

  # pos要求があるまで待機する。その後送信する
  # 要:通信切断対策
  def wait_and_send_position(pos)
    # posの受信待ち
    loop do
      msg=@network.receive_forever.chomp
      if msg=="pos" then
        output("Received position call")
        break
      end
    end
    @network.send_forever("pos #{pos.x} #{pos.y}")
    output("Sent position")
  end
  
  # ルート送信
  def send_route(point,route)
    msg="route*#{point.x},#{point.y}*#{route}"
    @network.send_forever(msg)
    output("Sent route")
  end
  
  # ルート受信
  def receive_route
    loop do
      msg=@network.receive_forever.chomp
      buf=msg.split("*")
      if buf[0].chomp=="route" then
        output("Received route")
        tmp=(buf[1].split(",")).map!{|item|item.to_f}
        p=Point.new(tmp[0],tmp[1])
        @area=Area.new(p)
        tmp=(buf[2].split(",")).map!{|item|item.to_i}
        @area.set_route(tmp)
        @area.show_area
        output("Route: #{tmp}")
        break
      end
    end
  end

  # 探索開始(自分の場所と探査ポイントの左上座標を引数に)
  def start_explore(my_position,point=nil)
    if @thread!=nil then
      @thread.kill
      output("terminated self monitoring")
    end
    if @thread2!=nil then
      @thread2.kill
      output("terminated sharing system")
    end
    if @thread3!=nil then
      @thread3.kill
      output("terminated receiving information")
    end
    
    if @network.is_parent? then
      start_parent_mode(point,my_position)
    else
      start_child_mode(my_position)
    end
    start_self_monitoring
    @finished_flag=@area.make_now_route
    @area.show_status
    start_sending_notify
    start_receiving_notify
    output("Ready to explore!")
    # ここはなんとかする
    loop do
      sleep(2)
      if @network.detect_lost_rover?&&@area.now!=nil then
        @flag1=@flag3=true # 受信停止
        output("\nWi-Fi of lost rover detected at the block #{@area.now} !! @ (#{@area.block_id(@area.now).center.x},#{@area.block_id(@area.now).center.y})\n")
        @area.discovered(@area.now)
        @area.show_status
        output("Completed!")
       # 相手は見つけ、自分は集合場所に集まった(ただし、検出できなかった) 
      elsif @finished_flag&&@area.target!=nil then
        output("\nReached to the lost rover point at the block #{@area.now} !! @ (#{@area.block_id(@area.target).center.x},#{@area.block_id(@area.target).center.y})\n")
        @area.show_status
        @finished_flag=false
      elsif @finished_flag then
        output("\nExplored all blocks.\nWe could not detect the lost rover.\nPress Ctrl+C to finish.")
        @area.show_status
        @finished_flag=false
        #raise Exception.new # 終了処理
      end
    end
  end

  # 親モードとして実行
  def start_parent_mode(point,my_position)
    output"Server mode start!"
    # ユーザーからpositionを受け取る
    if point==nil then
      output("Waiting for start point from user")
      puts("PORT: #{Parameter::ROUTE_PORT}(TCP), FORMAT: \"start lon(x) lat(y)\"")
      loop do 
        point=parse_pos("start",@network.receive_forever)
        if point!=nil then
          output("Received start point (#{point.x},#{point.y})")
          break
        end
      end
    end
    p=[my_position]
    if @network.get_client_ip.size>0 then
      p.push(get_position) # もう1台の位置取得
    end
    @area=Area.new(point)
    @area.show_area
    @area.generate_route(p)
    if @network.get_client_ip.size>0 then
      send_route(point,@area.get_clients_route)
    end
  end

  # 子モードとして実行
  def start_child_mode(my_position)
    output"Client mode start!"
    wait_and_send_position(my_position)
    receive_route
  end
  
  # tcpの監視開始
  def start_self_monitoring
    @thread=Thread::new do
      loop do
        @r2=TcpReceiver.new(Parameter::SELF_MONITORING_PORT)
        msg=@r2.receive.chomp
        if msg!=nil then
          buf=msg.split(" ")
          if buf.size==2&&buf[0]=="explored" then
            id=buf[1].to_i
            if !(id>=Parameter::BLOCK_SIZE_HORIZONTAL*Parameter::BLOCK_SIZE_VERTICAL||id<0) then
              output("Completed exploring block #{id}")
              @finished_flag=@area.explored(id,true)
            end
          end
        end
        if @flag1 then
          break
        end
      end
    end
  end
  
  # udpで情報垂れ流し
  def start_sending_notify
    @thread2=Thread::new do
      @s=UdpSender.new(Parameter::SHARING_PORT)
      loop do
        msg=@area.get_explored_block
        if msg!="" then
          @s.send("area "+msg)
        end
        if @flag2 then
          break
        end
        sleep Parameter::SHARING_INTERVAL
      end
    end
  end
  
  # udpから情報受取
  def start_receiving_notify
    @thread3=Thread::new do
      @r=UdpReceiver.new(Parameter::SHARING_PORT,nil)
      loop do
        begin
          msg=@r.receive_once
          if msg!=nil then
            buf=msg.chomp.split(" ")
            if buf.size==2 then
              if buf[0]=="area" then
                if @area.receive(buf[1].split(",").map{|item|item.to_i}) then
                  output("Another rover updated status.")
                  @finished_flag=@area.make_now_route
                  @area.show_status
                end
              end
            elsif buf.size==3 then # もう一方のローバーが見つけた場合
              if buf[0]=="area"&&@area.target==nil then
                t=buf[2].to_i
                output("Another rover reached to the lost rover at the block #{t} !! @(#{@area.block_id(t).center.x},#{@area.block_id(t).center.y})\nGoing to there.")
                @area.target=t
                @area.make_now_route
                @area.show_status
                @area.now=t
              end
            end
          end
        rescue
          sleep 0.1
        end
        if @flag3 then
          break
        end
      end
    end
  end
  
  # 終了処理
  def finish
    @r.close
    @s.close
    @r2.close
    @network.finish rescue nil
    @flag1=@flag2=@flag3=true
    @thread.kill rescue nil
    @thread2.kill rescue nil
    @thread3.kill rescue nil
  end
  
end
